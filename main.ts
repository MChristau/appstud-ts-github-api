import APIController from './controllers/APIController';
import ExampleController from './controllers/ExampleController';
import GitHubController from './controllers/GitHubController';
import UsersController from './controllers/UsersController';
import HttpGateway from './HttpGateway';

(async function main() {
  // Init Fastify router
  const http = new HttpGateway()

  // Controllers
  new ExampleController(http.router);
  new APIController(http.router);
  new UsersController(http.router);
  new GitHubController(http.router);

  // Fastify router start
  await http.start()
})()

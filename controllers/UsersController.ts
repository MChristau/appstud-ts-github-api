import { FastifyInstance, FastifyReply } from 'fastify';

import { ErrorResponse } from '../models/error-response.model';
import { UserPasswordRequest, UserTokenRequest } from '../models/user-request.model';
import { UserResponse, UserResponseWithToken } from '../models/user-response.model';
import { User, UserLoggedIn } from '../models/user.model';

export default class UsersController {
  private readonly baseURL: string = '/api/users';
  private router: FastifyInstance;
  private users: User[];
  private currentUser!: UserLoggedIn;

  constructor(router: FastifyInstance) {
    this.users = new Array<User>();
    this.router = router;

    /**
     * TASK-1003 : Register user
     */
    router.route({
      method: 'POST',
      url: this.baseURL + '/register',
      schema: {
        querystring: {
          username: { type: 'string' },
          password: { type: 'string' },
        }
      },
      handler: async (request: UserPasswordRequest, reply: FastifyReply) => {
        const userResponse = this.registerUser(request);

        if (userResponse.valueOf().hasOwnProperty('message')) {
          const error: ErrorResponse = userResponse as unknown as ErrorResponse;
          reply.status(error.status).send(error.message);
        }
        else reply.status(200).send(userResponse);
      }
    });

    /**
     * TASK-1004 : List registered users
     */
    router.get(this.baseURL + '',
      this.getRegisteredUsers.bind(this)
    );

    /**
     * TASK-1005 : Login the user
     */
    router.route({
      method: 'POST',
      url: this.baseURL + '/login',
      schema: {
        querystring: {
          username: { type: 'string' },
          password: { type: 'string' },
        }
      },
      handler: async (request: UserPasswordRequest, reply: FastifyReply) => {
        const userResponse = this.loginUser(request);

        if (userResponse.valueOf().hasOwnProperty('message')) {
          const error: ErrorResponse = userResponse as unknown as ErrorResponse;
          reply.status(error.status).send(error.message);
        }
        else reply.status(200).send(userResponse);
      }
    });

    /**
     * TASK-1006 : Get connected user
     */
    router.route({
      method: 'GET',
      url: this.baseURL + '/me',
      schema: {
        querystring: {
          token: { type: 'string' },
        }
      },
      handler: async (request: UserTokenRequest, reply: FastifyReply) => {
        const userResponse = this.getCurrentUser(request);

        if (userResponse.valueOf().hasOwnProperty('message')) {
          const error: ErrorResponse = userResponse as unknown as ErrorResponse;
          reply.status(error.status).send(error.message);
        }
        else reply.status(200).send(userResponse);
      }
    });
  }

  registerUser(request: UserPasswordRequest): UserResponseWithToken | ErrorResponse {
    const newUser: User = { ...request.query };

    const isUserExists = this.users.find((user) => user.username === newUser.username);

    if (!isUserExists) {
      this.users.push(newUser);

      const okResult: UserResponseWithToken = {
        username: newUser.username,
        token: this.generateToken(15)
      };

      this.currentUser = { ...okResult };

      return okResult;
    }

    return this.handleError('registerUser', { message: '[UsersController - registerUser] The user seems to already exist...', response: { status: 420 } });
  }

  async getRegisteredUsers(): Promise<UserResponse[]> {
    const registeredUsers: string[] = this.users.map(user => user.username);
    let result: UserResponse[] = [];

    for (let registeredUser of registeredUsers) {
      result.push({ username: registeredUser });
    }

    return result;
  }

  loginUser(request: UserPasswordRequest): UserResponseWithToken | ErrorResponse {
    const userToLogin: User = { ...request.query };
    const isUserExists = this.users.find((user) => (user.username === userToLogin.username) && (user.password === userToLogin.password));

    if (isUserExists) {
      const okResult: UserResponseWithToken = {
        username: userToLogin.username,
        token: this.generateToken(15)
      };

      this.currentUser = { ...okResult };

      return okResult;
    }

    return this.handleError('loginUser', { message: '[UsersController - loginUser] The user doesn\'t seems to exist or username/password are invalid...', response: { status: 401 } });
  }

  getCurrentUser(request: UserTokenRequest): UserResponse | ErrorResponse {
    const userToken: string = request.query.token;

    if (this.currentUser && this.currentUser.token === userToken) {
      const okResult: UserResponse = {
        username: this.currentUser.username,
      };

      return okResult;
    }

    return this.handleError('getCurrentUser', { message: '[UsersController - getCurrentUser] The user doesn\'t seems to exist or the token is invalid...', response: { status: 401 } });
  }

  /**
   * Generate a token with a parametric lentgth.
   * @param length Length of the token
   * @returns the generated token
   */
  private generateToken(length: number): string {
    const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    const charactersLength = characters.length;
    let generatedToken = '';

    for (let i = 0; i < length; i++) {
      generatedToken += characters.charAt(Math.floor(Math.random() * charactersLength));
    }

    return generatedToken;
  }

  private handleError(errorEmitter: string, error?: any): ErrorResponse {
    const defaultResult = '[UsersController - ' + errorEmitter + '] Something went wrong...';
    const errorResponse: ErrorResponse = {
      message: (error && error.message) ? error.message : defaultResult,
      status: (error && error.response && error.response.status) ? error.response.status : 500
    }

    return errorResponse;
  }
}

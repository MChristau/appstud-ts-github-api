import axios from 'axios';
import { FastifyInstance, FastifyReply, FastifyRequest } from 'fastify';

import { ErrorResponse } from '../models/error-response.model';
import { GitHubRequest } from '../models/github-request.model';
import {
    GitHubResponse, GitHubUserAPIResponse, GitHubUserResponse
} from '../models/github-response.model';
import { GitHubParams } from '../models/github.model';

export default class GitHubController {
  private readonly baseURL: string = '/api/github';
  private router: FastifyInstance;

  constructor(router: FastifyInstance) {
    this.router = router;

    /**
     * TASK-1007 : GitHub endpoint to retrieve feed
     */
    router.route({
      method: 'GET',
      url: this.baseURL + '/feed',
      handler: async (request: FastifyRequest, reply: FastifyReply) => {
        const githubResponse = await this.getGitHubFeed();

        if (githubResponse.valueOf().hasOwnProperty('message')) {
          const error: ErrorResponse = githubResponse as unknown as ErrorResponse;
          reply.status(error.status).send(error.message);
        }
        else reply.status(200).send(githubResponse);
      }
    });

    /**
     * TASK-1008 : GitHub endpoint to retrieve a public user with login
     */
    router.route({
      method: 'GET',
      url: this.baseURL + '/users/:actor_login',
      schema: {
        params: {
          actor_login: { type: 'string' },
        }
      },
      handler: async (request: GitHubRequest, reply: FastifyReply) => {
        const githubResponse = await this.getUserPublicData(request);

        if (githubResponse.valueOf().hasOwnProperty('message')) {
          const error: ErrorResponse = githubResponse as unknown as ErrorResponse;
          reply.status(error.status).send(error.message);
        }
        else reply.status(200).send(githubResponse);
      }
    });
  }

  async getGitHubFeed(): Promise<GitHubResponse[] | ErrorResponse> {
    return await axios.get<GitHubResponse[]>(
      'https://api.github.com/events',
      {
        headers: {
          Accept: 'application/json',
        },
      },
    ).then(response => {
      const okResult: GitHubResponse[] = [];

      for (let item of response.data) {
        const githubItem: GitHubResponse = {
          type: item.type,
          actor: {
            id: item.actor.id,
            login: item.actor.login
          },
          repo: {
            id: item.repo.id,
            name: item.repo.name
          }
        };

        okResult.push(githubItem);
      }

      return okResult;
    }).catch(error => {
      return this.handleError('getGitHubFeed', error);
    });
  }

  async getUserPublicData(request: GitHubRequest): Promise<GitHubUserResponse | ErrorResponse> {
    const requestParam: GitHubParams = (request.params as GitHubParams);

    return await axios.get<GitHubUserAPIResponse>(
      'https://api.github.com/users/' + requestParam.actor_login,
      {
        headers: {
          Accept: 'application/json',
        },
      },
    ).then(response => {
      const okResult: GitHubUserResponse = {
        id: response.data.id,
        login: response.data.login,
        avatar: response.data.avatar_url,
        details: {
          public_repos: response.data.public_repos,
          public_gists: response.data.public_gists,
          followers: response.data.followers,
          following: response.data.following
        }
      };

      return okResult;
    }).catch(error => {
      return this.handleError('getUserPublicData', error);
    });
  }

  private handleError(errorEmitter: string, error: any): ErrorResponse {
    const defaultResult = '[GitHubController - ' + errorEmitter + '] Something went wrong...';
    const errorResponse: ErrorResponse = {
      message: (error && error.message) ? error.message : defaultResult,
      status: (error && error.response && error.response.status) ? error.response.status : 500
    }

    return errorResponse;
  }
}

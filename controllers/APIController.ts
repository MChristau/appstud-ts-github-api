import axios from 'axios';
import { FastifyInstance, FastifyReply, FastifyRequest } from 'fastify';

import { DefaultResponse } from '../models/default-response.model';
import { ErrorResponse } from '../models/error-response.model';

export default class APIController {
  private readonly baseURL: string = '/api';
  private router: FastifyInstance;

  constructor(router: FastifyInstance) {
    this.router = router;

    /**
     * TASK-1001 : Check if the API is alive
     */
    router.route({
      method: 'GET',
      url: this.baseURL + '/healthcheck',
      handler: async (request: FastifyRequest, reply: FastifyReply) => {
        const apiResponse = await this.healthCheck();

        if (apiResponse.valueOf().hasOwnProperty('message')) {
          const error: ErrorResponse = apiResponse as unknown as ErrorResponse;
          reply.status(error.status).send(error.message);
        }
        else reply.status(200).send(apiResponse);
      }
    });

    /**
     * TASK-1002 : Tiny easter egg ;)
     */
    router.get(this.baseURL + '/timemachine/logs/mcfly',
      this.easterEgg.bind(this)
    );
  }

  async healthCheck(): Promise<DefaultResponse | ErrorResponse> {
    return await axios.get<DefaultResponse>(
      'https://api.github.com/zen',
      {
        headers: {
          Accept: 'application/json',
        },
      },
    ).then(data => {
      const okResult: DefaultResponse = {
        name: 'github-api',
        version: '1.0',
        time: new Date().getTime()
      };

      return okResult;
    }).catch(error => {
      return this.handleError('healthCheck', error);
    });
  }

  async easterEgg(): Promise<DefaultResponse[]> {
    const easterEgg: DefaultResponse[] = [
      {
        name: 'My mom is in love with me',
        version: '1.0',
        time: -446723100
      },
      {
        name: 'I go to the future and my mom end up with the wrong guy',
        version: '2.0',
        time: 1445470140
      },
      {
        name: 'I go to the past and you will not believe what happens next',
        version: '3.0',
        time: -Number.MAX_VALUE
      }
    ];

    return easterEgg;
  }

  private handleError(errorEmitter: string, error: any): ErrorResponse {
    const defaultResult = '[APIController - ' + errorEmitter + '] Something went wrong...';
    const errorResponse: ErrorResponse = {
      message: (error && error.message) ? error.message : defaultResult,
      status: (error && error.response && error.response.status) ? error.response.status : 500
    }

    return errorResponse;
  }
}

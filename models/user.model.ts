export type User = {
  username: string;
  password: string;
}

export type UserLoggedIn = {
  username: string;
  token: string;
}

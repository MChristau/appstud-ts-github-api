import { FastifyRequest } from 'fastify';

export type UserPasswordRequest = FastifyRequest<{
  Querystring: {
    username: string;
    password: string;
  }
}>

export type UserTokenRequest = FastifyRequest<{
  Querystring: {
    token: string;
  }
}>

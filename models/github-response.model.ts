export type GitHubResponse = {
  type: string;
  actor: {
    id: number,
    login: string
  };
  repo: {
    id: number,
    name: string
  };
}

export type GitHubUserResponse = {
  id: number;
  login: string;
  avatar: string;
  details: {
    public_repos: number;
    public_gists: number;
    followers: number;
    following: number;
  }
}

export type GitHubUserAPIResponse = {
  id: number;
  login: string;
  avatar_url: string;
  public_repos: number;
  public_gists: number;
  followers: number;
  following: number;
}

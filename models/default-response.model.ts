export type DefaultResponse = {
	name: string;
	version: string;
	time: number;
}

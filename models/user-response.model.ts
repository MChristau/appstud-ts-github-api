export type UserResponse = {
  username: string;
}

export type UserResponseWithToken = {
  username: string;
  token: string;
}

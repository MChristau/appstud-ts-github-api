import { FastifyRequest } from 'fastify';

export type GitHubRequest = FastifyRequest<{
  Querystring: {
    actor_login: string;
  }
}>

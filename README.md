# Appstud - Node.js with TypeScript

[See SUBJECT.md for the original subject](./SUBJECT.md)

## Getting started

- Clone this repository
- Use `npm install` to install dependencies
- Use `npm start` to run the project
- Open `http://127.0.0.1:3000/api` or `http://localhost:3000/api` url in your browser.
- You should see `Hello, friend`

## Available API routes
| Method | Description | API Route |
| ----------- | ----------- | ----------- |
| GET | Check if the API is alive | `http://localhost:3000/api/healthcheck` |
| GET | Roads ? Where we’re going, we don’t need roads | `http://localhost:3000/api/timemachine/logs/mcfly` |
| POST | Register a user with `[username]` and `[password]`. Returns a `token`. | `http://localhost:3000/api/register?username=[username]&password=[password]` |
| GET | List all registered users | `http://localhost:3000/api/users` |
| POST | Login the user with `[username]` and `[password]`. Returns a `token`. | `http://localhost:3000/api/users/login?username=[username]&password=[password]` |
| GET | Get the current connected user. Needs a `[token]`. | `http://localhost:3000/api/users/me?token=[token]` |
| GET | Get the public GitHub activity feed | `http://localhost:3000/api/github/feed` |
| GET | Retrieve a public user with `[actor_login]` | `http://localhost:3000/api/github/users/[actor_login]` |
